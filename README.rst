#########
Blobshare
#########

Blobshare (working title... maybe shellshare or something better)
allows to share file contents in a flow where the sender will use
blobshare to prepare a minimal amount of text, which the recipient
will paste in a command-line interface, so as to reconstruct the files
(the text includes basic shell commands such as mkdir, cat, gunzip).

This is very similar to `shar` except the amount of boilerplate is
minimized and the focus is to minimize the size of the generated text
blobs.

Because we're not in the 80's anymore, blobshare assumes everyone's
terminal is UTF-8. It also assumes by default that the recipient will
have gunzip, xz, zstd decompressors installed.


Example renditions:

.. code:: sh

   printf 'pouet ${echo} $(date)\n\n' > blobshare-a.txt
   printf '🍕\0\x1008f=\x8fff=\xff🍕tab=\t' > blobshare-b.txt
   cat << EOF | base64 -d > blobshare-c.txt
   YdWrLoZNQfIN1XxqK2QkbmgFYO0Q56J4aQeNarT85yaGdznEFWAcghqQdaujtvMCl3jS8rpvjS2Q+5dN
   eEXbLpcSo6wfCg==
   EOF
   printf 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIn2dxnsuQFOmMlZZmpr021KqRvz1m/I6kmNdc4R921O cJ@Vantage\n' > blobshare-e.pub
   cat << EOF | base64 -d | zstd -d > blobshare-f.dat
   KLUv/QAA/QEAJAMAMTIzAQIDBAUGBwgJCgsMDQ4PEBESExQVFhcYGRobHB0eHyAhIiMkJSYnMTIzKDEy
   MyakEPBz4A8AYCyl
   EOF

A compression tool (or none) is selected to minimize the printed size;
writing to a destination file can be done using printf or cat with a
*here document*, whichever produces the smallest command line.


Usage
#####

Essentially you run:

.. code:: sh

   blobshare ${file-to-share}

See `example.sh` for an example.

.. include:: example.sh
  :code: sh

