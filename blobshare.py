#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# File sharing tool
# SPDX-FileCopyrightText: 2021-2025 Jérôme Carretero <cJ-blobshare@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import logging, io, os, subprocess, base64, tempfile, shlex, re
import sys
import contextlib

logger = logging.getLogger(__name__)


def packmin(blob,
 use_brotli=False,
 use_zopfli=True,
 use_zstd=True,
 use_gzip=True,
 use_bzip2=True,
 use_bzip3=True,
 use_xz=True,
 ):
	with tempfile.NamedTemporaryFile() as fp:
		fp.write(blob)
		fp.flush()

		commands = []

		if use_zstd:
			ss = f"--stream-size={len(blob)} --no-check --no-dictID"
			for o in (f"{ss}", f"{ss} --single-thread"):
				for i in range(1, 19+1):
					commands += [(f"zstd --no-content-size -{i} {o}", "zstd -d")]
				for i in range(20, 22+1):
					commands += [(f"zstd --no-content-size --ultra -{i} {o}", "zstd -d")]

		if use_xz:
			for i in range(0, 9+1):
				commands += [(f"xz --check=none -{i}", "xz -d")]

		if use_gzip and not use_zopfli:
			for i in range(1, 9+1):
				commands += [(f"gzip -{i}", "gunzip")]

		if use_bzip2:
			for i in range(1, 9+1):
				commands += [(f"bzip2 -{i}", "bunzip2")]

		if use_bzip3:
			for i in range(1, 9+1):
				commands += [(f"bzip3 --block {i}", "bunzip3")]

		if use_brotli:
			for i in range(1, 9+1):
				commands += [(f"brotli -{i}", "brotli -d")]

		if use_zopfli:
			commands += [(f"zopfli -i9999 -c {fp.name}", "gunzip")]

		table = {None: blob}
		minc = None
		minv = len(blob)
		mind = ""

		for command, command_d in commands:

			res = subprocess.run(command,
			 shell=True,
			 input=blob,
			 stdout=subprocess.PIPE,
			)

			if res.returncode != 0:
				logger.warning("Error (%d) in pack attempt “%s”",
				 res.returncode, command)
				continue

			table[command] = res.stdout
			l = len(res.stdout)

			logger.debug("- %s -> %d", command, l)

			if l < minv:
				minc = command
				minv = l
				mind = f" | {command_d}"

	return table[minc], mind

def as_printf(z):
	u = z.decode("utf-8", "surrogateescape")
	m = {}
	for i in range(0x20):
		m[i] = f"\\x{i:02x}"
	m.update({
	 ord("\a"): r"\a",
	 ord("\b"): r"\b",
	 8: r"\b",
	 ord("\f"): r"\f",
	 ord("\n"): r"\n",
	 ord("\r"): r"\r",
	 ord("\t"): r"\t",
	 ord("\v"): r"\v",
	 ord('%'): '%%',
	 0x1b: r"\e",
	})
	for i in range(0x80, 0xff+1):
		m[0xdc00+i] = f"\\x{i:02x}"

	u = u.translate(m)

	def octrepl(m):
		x = int(m.group(0)[2:], 16)
		if x < 0x20:
			return f"\\{x:o}"
		else:
			return m.group(0)

	u = re.sub(r'(\\x[0-9a-f]{2})(?![0-9])', octrepl, u)

	return shlex.quote(u)

def as_base64(z: bytes) -> str:
	lines = []
	s = base64.b64encode(z)
	n = 80
	for i in range(0, len(s), n):
		lines += [s[i:i+n].decode()]
	return "\n".join(lines)


def is_exe(fpath: str) -> bool:
	return os.path.isfile(fpath) and os.access(fpath, os.X_OK)


def check_exe(name, env=None):
	"""
	Ensure that a program exists
	:type name: string
	:param name: name or path to program
	:return: path of the program or None
	"""
	if env is None:
		env = os.environ

	if not name:
		raise ValueError('Cannot execute an empty string!')
	def is_exe(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

	fpath, fname = os.path.split(name)
	if fpath and is_exe(name):
		return os.path.abspath(name)
	else:
		for path in env["PATH"].split(os.pathsep):
			path = path.strip('"')
			exe_file = os.path.abspath(os.path.join(path, name))
			if is_exe(exe_file):
				return exe_file
	return None


def can_use(use: str):
	return check_exe(use) is not None


if __name__ == "__main__":

	import argparse

	parser = argparse.ArgumentParser(
	 description="Blobshare -- create blobs for smallish files sharing",
	)

	log_level = os.environ.get("BLOBSHARE_LOG_LEVEL", "INFO")
	parser.add_argument("--log-level",
	 default=log_level,
	 choices=("DEBUG", "INFO", "WARNING", "ERROR"),
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--escape",
	 choices=("markdown", "md"),
	 help="Escape printing",
	)

	possible_uses = (
	 "gzip",
	 "bzip2",
	 "bzip3",
	 "xz",
	 "zstd",
	 "zopfli",
	 "brotli",
	)

	default_use = []
	for use in possible_uses:
		if can_use(use):
			default_use.append(use)
	default_use_str = os.environ.get("BLOBSHARE_USE", ",".join(default_use))

	parser.add_argument("--use",
	 help="Consider these compression programs (default {})".format(
	  default_use_str),
	 default=default_use_str,
	)

	parser.add_argument("--root",
	 help="Consider paths relative to this root. Defaults to arg dirname.",
	)

	parser.add_argument("path",
	 default=[],
	 nargs="*",
	)

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	files = set()
	for path in args.path:
		if os.path.isdir(path):
			for cwd, ds, fs in os.walk(path):
				for f in fs:
					path = os.path.join(cwd, f)
					files.add(path)
		else:
			files.add(path)

	with contextlib.ExitStack() as stack:

		if not files:
			data = sys.stdin.buffer.read()

			tmpdir = tempfile.TemporaryDirectory(
			 prefix="blobshare",
			)
			stack.enter_context(tmpdir)

			p = os.path.join(tmpdir.name, "stdin")
			with io.open(p, "wb") as fo:
				fo.write(data)
			files.add(p)

		cmd_uses = set(args.use.split(","))
		kw = dict()
		for use in possible_uses:
			if use in cmd_uses:
				if use not in default_use:
					logger.warning("Can't use %s", use)
					value = False
				else:
					value = True
				kw[f"use_{use}"] = value
			else:
				kw[f"use_{use}"] = False

		contents = []
		for path in sorted(files):
			dirname, basename = os.path.split(path)
			relpath = basename if args.root is None else os.path.relpath(path, args.root)
			relparent = os.path.dirname(relpath)

			with io.open(path, "rb") as fi:
				z, d = packmin(fi.read(), **kw)

			candidates = [
			 f"printf {as_printf(z)}{d} > {relpath}",
			 f"cat << EOF | base64 -d{d} > {relpath}\n{as_base64(z)}\nEOF",
			]
			contents += [(relparent, sorted(candidates, key=len)[0]),]

			if os.access(path, os.X_OK):
				contents += [(relparent, f"chmod +x {relpath}"),]

		out = []
		dircmds = {''}
		for parent, s in contents:
			if not parent in dircmds:
				out += ["mkdir -p {}".format(shlex.quote(parent))]
				dircmds.add(parent)
			out += [s]
		s = "\n".join(out)

		if args.escape in ("md", "markdown"):
			s = "```sh\n" + s + "\n```"

		print(s)
