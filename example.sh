#!/bin/sh

mkdir -p reference received
cd reference
printf '\1❤️\n✅\n✨\n🔥\3\n😊\n😂\xff\n🫶\n💀\n😍' > a.txt
cd ../received
python ../blobshare.py ../reference/* | sh
cd ..
diff -r reference received
